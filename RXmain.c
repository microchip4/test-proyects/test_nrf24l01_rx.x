/*
 * File:   RXmain.c
 * Author: pic18fxx.blogspot.com
 * 
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "Alteri.h"
//#include <pic18f46k22.h>
#include <plib/spi.h>
//#include "bit_settings.h"
#include "nrf24l01.h"
#include "hd44780.h"
//#include "my_delays.h"

void main(void) {
    unsigned char bufferRX[32];
    ADCON0=0x00; //all digital
    ADCON1=0x00; //all digital
    ADCON1bits.PCFG=0x0F;
    //__delay_ms(100);  // Allow time for Vdc to settle on GLCD
    TRISB = 0x00;       //Set data port  B as output
    TRISD = 0x00;       // Set control port D as output
    /*
    OSCCON = 0b01110000;
    OSCTUNEbits.PLLEN = 1; // turn on the PLL 64 MHz
    ANSELA = 0; ANSELB = 0; ANSELC = 0; ANSELD = 0; ANSELE = 0;
    PORTA  = 0; PORTB  = 0; PORTC  = 0; PORTD  = 0; PORTE  = 0;
    TRISA  = 0; TRISB  = 0; TRISC  = 0; TRISD  = 0; TRISE  = 0;
     * */
    CloseSPI();
    OpenSPI(SPI_FOSC_64, MODE_00, SMPEND);

    NRF24L01_Init(RX_MODE, 0x40); 
    while(1){
        while(NRF24L01_DataReady()){}
        NRF24L01_ReadData(bufferRX);
        LATB = bufferRX[0];
    }
    while(1){}
}